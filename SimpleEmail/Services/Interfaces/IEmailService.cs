﻿using SimpleEmail.Dto.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleEmail.Services.Interfaces
{
    public interface IEmailService
    {
        Task<string> Send(SendEmailInDto sendEmailInDto);
    }
}
