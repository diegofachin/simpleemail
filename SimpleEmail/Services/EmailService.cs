﻿using SimpleEmail.Dto.Email;
using SimpleEmail.Enum;
using SimpleEmail.Services.Interfaces;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SimpleEmail.Services
{

    public class EmailService : IEmailService
    {
        public Task<string> Send(SendEmailInDto sendEmailInDto)
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "simpleemail2709@gmail.com",
                        Password = "simple2709"
                    };

                    client.Credentials = credential;
                    client.Host = SmtpHelper.GetDescription(Smtp.gmail);
                    client.Port = (int)Smtp.gmail;
                    client.EnableSsl = true;

                    using (var emailMessage = new MailMessage())
                    {
                        emailMessage.To.Add(new MailAddress(sendEmailInDto.Email));
                        emailMessage.From = new MailAddress("simpleemail2709@gmail.com");
                        emailMessage.Subject = sendEmailInDto.Subject;
                        emailMessage.Body = sendEmailInDto.Body;
                        client.Send(emailMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                return Task.FromResult(ex.ToString());
            }

            return Task.FromResult("Enviado com sucesso!!");
        }
    }
}
