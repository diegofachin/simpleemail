﻿using Microsoft.AspNetCore.Mvc;
using SimpleEmail.Dto.Email;
using SimpleEmail.Services.Interfaces;

namespace SimpleEmail.Controllers
{
    [Route("api/[controller]")]
    public class EmailController : Controller
    {
        private IEmailService _emailService;
        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("Send")]
        [ProducesResponseType((201), Type = typeof(SendEmailInDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        //[Authorize("Bearer")]
        public IActionResult SendEmail([FromBody] SendEmailInDto sendEmailInDto)
        {
            sendEmailInDto.Email = "diegofachin@gmail.com";
            sendEmailInDto.Message = "teste";
            sendEmailInDto.Subject = "teste x";
            sendEmailInDto.Body = "corpo de teste para envio docker";

            var send = _emailService.Send(sendEmailInDto);
            return Ok(send);
        }

    }
}
