﻿using System.ComponentModel;

namespace SimpleEmail.Enum
{
    public enum Smtp
    {
        [Description("smtp.gmail.com")]
        gmail = 465
    }

    public static class SmtpHelper
    {
        public static T GetAtributteOfType<T>(this Smtp EnumValue) where T : System.Attribute
        {
            var type = EnumValue.GetType();
            var memInfo = type.GetMember(EnumValue.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        public static string GetDescription(this Smtp valorEnum)
        {
            return valorEnum.GetAtributteOfType<DescriptionAttribute>().Description;
        }
    }
}
